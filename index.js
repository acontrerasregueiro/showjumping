 'use strict'

var rutas = require('./server-modules/rutas.js') // contiene la resolucion de rutas
 //contiene las operaciones sobre la bbdd Table_Jinetes
var bbddjinetes = require('./server-modules/operaciones-bbddjinete.js')
var bbddcaballos = require('./server-modules/operaciones-bbddcaballo.js')
var bbddcompeticiones = require('./server-modules/operaciones-bbddcompeticiones.js')
var bbddpruebas = require('./server-modules/operaciones-bbddpruebas.js')
var bbddpruebaencurso = require('./server-modules/operaciones-bbddpruebaactiva.js')
// modulo de envio de ordenes mostrar diferentes pantallas videowall
var socketvideowall = require('./server-modules/videowall/socketvideowall.js') 
// var login = require('./server-modules/login.js')
var serialport = require('./server-modules/operaciones-serialport.js')
var marcador = require('./server-modules/videowall/marcador.js')

var funciones = require('./server-modules/funciones.js')
var csvtojson = require('./server-modules/operaciones-csvjson.js')
var csvtojsongallego = require('./server-modules/operaciones-csvjsongallego.js')

var express = require('express')
var path = require('path')
// var Stream = require('stream')
var app = express()
const bodyParser = require("body-parser");

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
    extended: true
}))

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());


var assert = require('assert')
var http = require('http').Server(app)
var io = require('socket.io')(http)
const MongoClient = require('mongodb').MongoClient

// Connection URL
const url = 'mongodb://localhost:27017'

// Database Name
const dbName = 'hipica'



http.listen(9000) // Iniciamos el servidor
rutas(app,path,express)

 console.log('Server listening on Port: 9000')

 console.log( io.sockets.sockets )

var contador = 1
// io.sockets.sockets.map(function(e) {
//     console.log(e.username)
// })
// Object.keys(io.sockets.sockets).forEach(function (s){
//     console.log(io.sockets.sockets[s])
//     io.sockets.sockets[s].disconnect(true) 
// })
var sockets = {}, nextSocketId = 0;
io.on('connection', function (socket) {
    var socketId = nextSocketId++;
    sockets[socketId] = socket;
    console.log('socket', socketId, 'opened');

    // io.of('/').adapter.clients(function (err, clients) {
           
    //     for (var indice = 0; indice < clients.length; indice ++ )
    //     {
    //         console.log(indice + '    > ' + clients[indice])
    //         io.of('/').adapter.remoteDisconnect(clients[indice], true, (err) => {
    //             if (err) { /* unknown id */ }
    //             // success
    //           })         
    //     }
    //     // clients.forEach(
    //   })
    // socket.removeListener('testComplete');
    console.log(contador)
    bbddjinetes(socket,MongoClient,url,dbName)
    bbddcaballos(socket,MongoClient,url,dbName)
    bbddcompeticiones(socket,MongoClient,url,dbName)
    bbddpruebas(socket,MongoClient,url,dbName)
    bbddpruebaencurso(socket,MongoClient,url,dbName)
    
    csvtojson(socket,MongoClient,url,dbName)
    csvtojsongallego(socket)
    socketvideowall(socket)
    marcador(socket)
   
    // // if (contador == 1) {
        serialport(socket)
        serialport.iniciarpuertosserie()
        contador = contador + 1

         
          
        // socket.disconnect(true)
        // socket.disconnect()
    // }
    // console.log(contador)
    // io.close()
// // login(socket)
//     // io.sockets.on('connection', function(socket) {
//         socketlist.push(socket)
//         socket.emit('socket_is_connected','You are connected!')
//         socket.on('close', function () {
//         console.log('socket closed')
//         socketlist.splice(socketlist.indexOf(socket), 1)
//         })
//     // })
//     socketlist.forEach(function(socket) {
//         socket.disconnect()
//       })
})

