
'use strict'
var funcionescomunes = require('./funciones-compartidas.js')
var funcionespruebas = require('./funciones-pruebas.js')
var ordenartabla = require('./sorttable.js')// Al hacer click en el encabezado de tabla ordena la tabla por ese campo
var clasificarcampeonato = require('./clasificarbaremoscampeonato.js')
var socket = io()

//MODULO PARA EL CAMPEONATO GALLEGO
function anadirfila (participante, indice,baremo) {

var tbodyempezarprueba = document.getElementById('tbodyempezarprueba')
  var newrow = tbodyempezarprueba.insertRow(-1)
  var celdaseleccionarparticipante = newrow.insertCell(-1)
  celdaseleccionarparticipante.id = 'participante' + indice + 'Seleccion'
  celdaseleccionarparticipante.classList.add('tablecursor')
  newrow.id = 'filapruebaactiva' + indice
  var celdaorden = newrow.insertCell(-1)
  celdaorden.id = 'participante' + indice + 'Orden'
  celdaorden.classList.add('tableorden')
  var newText = document.createTextNode(participante.orden)
  celdaorden.appendChild(newText)
  var celdacaballo = newrow.insertCell(-1)
  celdacaballo.id = 'participante' + indice + 'Caballo'
  celdacaballo.classList.add('tablecaballo')
  var newText = document.createTextNode(participante.caballo)
  celdacaballo.appendChild(newText)
  var celdajinete = newrow.insertCell(-1)
  celdajinete.id = 'participante' + indice + 'Jinete'
  celdajinete.classList.add('tablejinete')
  var newText = document.createTextNode(participante.jinete)
  celdajinete.appendChild(newText)
  var celdarank = newrow.insertCell(-1)
  celdarank.id = 'participante' + indice + 'Class'
  celdarank.classList.add('tableorden')
    newText = document.createTextNode('') // si no tiene ranking no pintamos nada

  celdarank.appendChild(newText)
  var celdapuntos = newrow.insertCell(-1)
  celdapuntos.id = 'participante' + indice + 'Penalidades'
  celdapuntos.classList.add('tablepuntos')
  var newText = document.createTextNode(participante.puntos)
  celdapuntos.appendChild(newText)
  var celdatiempo = newrow.insertCell(-1)
  celdatiempo.id = 'participante' + indice + 'Tiempo'
  celdatiempo.classList.add('tabletiempo')
  var newText = document.createTextNode(participante.tiempo)
  celdatiempo.appendChild(newText)
  celdatiempo.classList.add('tableclasificar')
  if (baremo == '2') {
    var celdapuntos2 = newrow.insertCell(-1)
    celdapuntos2.id = 'participante' + indice + 'Penalidades2'
    celdapuntos2.classList.add('tablepuntos')
    var newText = document.createTextNode(participante.puntos2)
    celdapuntos2.appendChild(newText)

    var celdatiempo2 = newrow.insertCell(-1)
    celdatiempo2.id = 'participante' + indice + 'Tiempo2'
    celdatiempo2.classList.add('tabletiempo')
    var newText = document.createTextNode(participante.tiempo2)
    celdatiempo2.appendChild(newText)
    celdatiempo2.classList.add('tableclasificar')

    var celdapuntostotal = newrow.insertCell(-1)
    celdapuntostotal.id = 'participante' + indice + 'PenalidadesTotal'
    celdapuntostotal.classList.add('tableorden')
    var newText = document.createTextNode('')
    celdapuntostotal.appendChild(newText)
    celdapuntostotal.classList.add('tableclasificar')

    var celdaranktotal = newrow.insertCell(-1)
    celdaranktotal.id = 'participante' + indice + 'RankTotal'
    celdaranktotal.classList.add('tableorden')
    var newText = document.createTextNode('')
    celdaranktotal.appendChild(newText)
    celdaranktotal.classList.add('tableclasificar')
    
    var celdaclasscampeonato = newrow.insertCell(-1)
    celdaclasscampeonato.id = 'participante' + indice + 'PuntosCampeonato'
    celdaclasscampeonato.classList.add('tableorden')
    var newText = document.createTextNode(participante.PuntosCampeonato)
    celdaclasscampeonato.appendChild(newText)
    celdaclasscampeonato.classList.add('tableclasificar')
  }
  newrow.addEventListener('click', function () {
    funcionescomunes.borrarclase('text-primary', this.parentNode)//eliminamos la clase bgsuccess del nodopadre(color)
    newrow.classList.add('text-primary')
    funcionespruebas.actualizarjineteenpista(newrow.id)
  })
  newrow.oncontextmenu = function(e) {
    var contextmenu = document.getElementById('context-menu')
 	  e.preventDefault();
    contextmenu.style.left = e.pageX + 'px';
    contextmenu.style.top = e.pageY + 'px';
    contextmenu.style.display = 'inline-block';
 }
}
function deleteColumn(tblId) {
  var row = document.getElementById('filaencabezadoempezarprueba')
  row.deleteCell(-1)
}

function resetearcolumnasencabezado() {
  var numerodecolumnas = document.getElementById('tablaempezarprueba').rows[0].cells.length
  if ((numerodecolumnas == 10)) {
    //baremo previo era un A o C, hay que eliminar 3 columnas
    //tiempo, puntos de la segunda fase y total.
    deleteColumn('filaencabezadoempezarprueba')
    deleteColumn('filaencabezadoempezarprueba')
    deleteColumn('filaencabezadoempezarprueba')
  
  } else if (numerodecolumnas > 10) {
    deleteColumn('filaencabezadoempezarprueba')
    deleteColumn('filaencabezadoempezarprueba')
    deleteColumn('filaencabezadoempezarprueba')
    deleteColumn('filaencabezadoempezarprueba')
    deleteColumn('filaencabezadoempezarprueba')
  }
}

function addColumn(tblId)
{
  //PRIUMERO MODIFICIAMOS EL ENCABEZADO DE LA TABLA
  var tblHeadObj = document.getElementById(tblId).tHead;
  for (var h=0; h<tblHeadObj.rows.length; h++) {
    var newTH = document.createElement('th') //puntos segunda fase
    var newTH2 = document.createElement('th')//tiempo segunda fase
    var newTH3 = document.createElement('th')//total puntos
    var newTH4 = document.createElement('th')//total class campeonato
    var newTH5 = document.createElement('th')//total puntos campeonato
    var newTH6 = document.createElement('th')//class cpto


    tblHeadObj.rows[h].appendChild(newTH)
    newTH.id = 'encabezadoPuntos2'
    newTH.classList.add('tablepuntos')
    newTH.innerHTML = 'Pto 2ª'
    tblHeadObj.rows[h].appendChild(newTH2)
    newTH2.id = 'encabezadoTiempo2'
    newTH2.classList.add('tabletiempo')
    newTH2.innerHTML = 'Tpo 2ª'
    tblHeadObj.rows[h].appendChild(newTH3)
    newTH3.id = 'encabezadoPenalidadesTotal'
    newTH3.classList.add('tableorden')
    newTH3.innerHTML = 'Ttal Prueba'
    tblHeadObj.rows[h].appendChild(newTH4)
    newTH4.id = 'encabezadoClasscampeonato'
    newTH4.classList.add('tableorden')
    newTH4.innerHTML = 'Rank Cto'
    tblHeadObj.rows[h].appendChild(newTH5)
    newTH5.id = 'encabezadoTotalcampeonato'
    newTH5.classList.add('tableorden')
    newTH5.innerHTML = 'TtalPto'
    
  }
}
function leerclasificacionmanual(socket){
  var filas = document.getElementById('tablaempezarprueba').rows.length
  var arrayptoscto = []

  for (var indice = 0; indice < filas -1; indice++) {
    var participante = {}
    var caballo = document.getElementById('participante' + indice+ 'Caballo')
    var jinete = document.getElementById('participante' + indice + 'Jinete')
    var puntoscto = document.getElementById('participante' + indice + 'PuntosCampeonato')
    var clasificacion = document.getElementById('participante' + indice + 'RankTotal')
    // var Penalidades2 = document.getElementById('participante' + indice + 'Penalidades2')
    // var Tiempo2 = document.getElementById('participante' + indice + 'Tiempo2')

    participante.caballo = caballo.innerHTML
    participante.jinete = jinete.innerHTML
    participante.puntoscto = puntoscto.innerHTML
    participante.clasificacion = clasificacion.innerHTML
    if (participante.clasificacion == 'ELI') {
      participante.puntoscto = '555'
      // participante.clasificacion = null
    }
    if (participante.clasificacion == 'RET') {
      participante.puntoscto = '666'
      // participante.clasificacion = null    
    }    
    arrayptoscto.push(participante)
  }
  
  arrayptoscto.sort(
    firstBy('clasificacion'))
  enviarencabezadoclasificacion(socket)
  console.log(arrayptoscto)
  socket.emit('websocketclasificarcampeonato',arrayptoscto)
  // return arrayptoscto
}
function leerpuntoscto(){
  var filas = document.getElementById('tablaempezarprueba').rows.length
  var arrayptoscto = []

  for (var indice = 0; indice < filas -1; indice++) {
    var participante = {}
    var caballo = document.getElementById('participante' + indice+ 'Caballo')
    var jinete = document.getElementById('participante' + indice + 'Jinete')
    var puntoscto = document.getElementById('participante' + indice + 'PuntosCampeonato')
    var Penalidades2 = document.getElementById('participante' + indice + 'Penalidades2')
    var Tiempo2 = document.getElementById('participante' + indice + 'Tiempo2')

    participante.caballo = caballo.innerHTML
    participante.jinete = jinete.innerHTML
    participante.puntoscto = puntoscto.innerHTML
    participante.puntos2 = Penalidades2.innerHTML
    participante.tiempo2 = Tiempo2.innerHTML
    arrayptoscto.push(participante)
  }
  return arrayptoscto
  
  socket.emit('websocketclasificarcampeonato',arraytotal)
}
function asignarrankingeliminadoscampeonato(arraynoclasificados) {
 //666 RET
 //555 ELI
    for (var i= 0; i < arraynoclasificados.length; i++) {
      var celdaclass = funcionespruebas.buscarentablacampeonato('tablaempezarprueba',arraynoclasificados[i].caballo,arraynoclasificados[i].jinete)
        if (arraynoclasificados[i].puntoscto == '555') {celdaclass.innerHTML = 'ELI'}
        if (arraynoclasificados[i].puntoscto == '666') {celdaclass.innerHTML = 'RET'}       
    }  
}
function asignarrankingatablacampeonato(arrayordenado) {
  for (var i= 0; i < arrayordenado.length; i++) {
    // alert('clasificando  ' + arrayordenado[i].caballo)
    
    var celdaclass = funcionespruebas.buscarentablacampeonato('tablaempezarprueba',arrayordenado[i].caballo,arrayordenado[i].jinete)
    // alert('arrayordenado.length : ' + arrayordenado.length + ' Vuelta nº : ' + i + 'arrayordenado[i]  : ' + arrayordenado[i].caballo + '   CeldaClass   : ' + celdaclass.id)
    celdaclass.innerHTML = arrayordenado[i].clasificacion
  }
}
function enviarencabezadoclasificacion(socket) {
  var labeltrofeoempezarprueba = document.getElementById('labeltrofeoempezarprueba').innerHTML
  var labelalturaempezarprueba = document.getElementById('labelalturaempezarprueba').innerHTML
  var labelbaremoempezarprueba = document.getElementById('labelbaremoempezarprueba').innerHTML
  socket.emit('encabezadoclasificaciondeprueba',labelalturaempezarprueba,labeltrofeoempezarprueba,labelbaremoempezarprueba)
}

function iniciarcampeonato(socket) {

  var btnenviarcampeonato = document.getElementById('btnenviarcampeonato')
  btnenviarcampeonato.addEventListener('click', function(){
    leerclasificacionmanual(socket)
  })
 
  var btnclasificarcto = document.getElementById('btnclasificarcto')
  btnclasificarcto.addEventListener('click',function (){
   var arrayptoscto = leerpuntoscto()
   var arraynoclasificados = []
   arrayptoscto.sort(
    firstBy (function (v1, v2) { return v1.puntoscto - v2.puntoscto })
    // .thenBy('caballo', -1)
    // .thenBy(function (v1, v2) { return v1.puntos2 - v2.puntos2 })
    // .thenBy(function (v1, v2) { return v1.tiempo2 - v2.tiempo2 })
  )
  console.log('ARRAY DESPUES DE ORDENAR POR PTOSCTO :  ' ,arrayptoscto)
  var arrayordenado = []
  var arraynoclasificados = []
  var filas = document.getElementById('tablaempezarprueba').rows.length
  for (var i= 0; i < filas -1; i++) {
    // alert(arrayptoscto[i].puntoscto)
    if(arrayptoscto[i].puntoscto == 555) { //ELIMNADOP
      arraynoclasificados.push(arrayptoscto[i])
      // alert('ENCONTRado eliminado   : ' , arrayptoscto[i])
      // alert('JINETE ELIMNADO O NO RETIRADO DE CAMPEONATO 999')

    } 
    else if(arrayptoscto[i].puntoscto == 666) { //RETIRADO
      arraynoclasificados.push(arrayptoscto[i])
      // alert('JINETE ELIMNADO O NO RETIRADO DE CAMPEONATO 9999')
      // alert('ENCONTRado eliminado   : ' , arrayptoscto[i])
    }
    else {
      arrayordenado.push(arrayptoscto[i])
    }
  }
  
  clasificarcampeonato.asc(arrayptoscto)
  console.log('array eliminados : ', arraynoclasificados)
  console.log('puntos cto ' ,arrayptoscto)
  console.log('array ordenado  : ', arrayordenado)
  var arraytotal =[]
  // arraytotal = arrayordenado.concat(arraynoclasificados)
  // var arraytotal =[]
  arraytotal = arrayordenado.concat(arraynoclasificados)
  // console.log(' array total : ', arraytotal)
  asignarrankingatablacampeonato(arrayordenado)
  asignarrankingeliminadoscampeonato(arraynoclasificados)
  enviarencabezadoclasificacion(socket)
  socket.emit('websocketclasificarcampeonato',arraytotal)
 
  })

  var navimportarcsv = document.getElementById('navimportarcsv')
  navimportarcsv.addEventListener('change', function (){
    // var thefile = document.getElementById('navimportarcsv')
    var filename = navimportarcsv.files[0].name
    alert(filename)
      socket.emit('importarcsvgallego', filename) 
  })
   socket.on('importarordendesalidagallego',function(data){

      var arrayparticipantes = []
      data.forEach(function (binomio){
        console.log(binomio)
        var objetoparticipante = {}
        objetoparticipante.jinete = binomio.Jinete
        objetoparticipante.caballo = binomio.Caballo
        objetoparticipante.puntos = ''
        objetoparticipante.tiempo = ''
        objetoparticipante.puntos2 = ''
        objetoparticipante.tiempo2 = ''
        objetoparticipante.rank = ''
        objetoparticipante.rankcampeonato = ''        
        objetoparticipante.PuntosCampeonato = binomio.TtalPto

        if ((binomio.TtalPto == null) || (!binomio.TtalPto)) {

          objetoparticipante.PuntosCampeonato = binomio.Puntos
        } else {
          objetoparticipante.PuntosCampeonato = binomio.TtalPto
        }
        arrayparticipantes.push(objetoparticipante)
      })
        
        for (var indice = 0; indice < arrayparticipantes.length; indice++) {
          var participante = arrayparticipantes[indice]
          participante.orden = indice +1
          var baremo = 2
          anadirfila(participante, indice,baremo)
        }  
        resetearcolumnasencabezado()
         addColumn('tablaempezarprueba')
         var encabezadoTotalcampeonato = document.getElementById('encabezadoTotalcampeonato')
         encabezadoTotalcampeonato.addEventListener('click',function(){
           alert('ord3enando')
           ordenartabla.sortTable(11)
        }) 
      })
 
   }
  //  socket.emit('websocketclasificar',arraytotal)
   
module.exports.iniciarcampeonatogallego = function(socket) {
 iniciarcampeonato(socket)
}
