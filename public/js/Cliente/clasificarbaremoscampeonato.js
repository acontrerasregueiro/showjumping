/*Fichero en el que se realizan todas las operaciones relacionadas
con las bases de datos de competiciones*/
'use strict'

// var funcionescomunes = require('./funciones-compartidas.js')
// var funcionesformulariocompeticionseleccionada = require('.//operaciones-formulario-competicionseleccionada.js')


module.exports.asc = function(array) {
  var clasificacion = 1
  array[0].clasificacion = clasificacion
  for (var i = 0; i < (array.length - 1) ; i++ ) {
    if(array[i].puntoscto == array[i + 1].puntoscto) {
      array[i +1].clasificacion = array[i].clasificacion
    } else {
      array[i + 1].clasificacion = i + 2
      // clasificacion = i + 1
    }
  }
}
module.exports.acc = function(array) {
  var clasificacion = 1
  array[0].clasificacion = clasificacion
  for (var i = 0; i < (array.length - 1) ; i++ ) {
    if ((array[i].puntos == array[i + 1].puntos) && (array[i].tiempo == array[i + 1].tiempo)) {
      array[i +1].clasificacion = array[i].clasificacion
    } else {
      array[i + 1].clasificacion = i + 2
      // clasificacion = i + 1
    }
  }
}
