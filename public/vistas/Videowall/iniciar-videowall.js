'use strict'
/*ESTE MODULO SE ENCARGA DEL ENVIO DE ORDENES A TRAVES DE SOCKET
PARA MOSTRAR LAS DIFERENTES PANTALLAS DEL VIDEO WALL*/
// var socket = io ()

// var clasificaciontopten = require('../Videowall/clasificaciontopten.js')

function iniciarvideowall(socket) {
    
    var Videowallrun = document.getElementById('Videowallrun')  // LANZA LA PAGINA WEB DEL VIDEOWALL
    var Videowallorden = document.getElementById('Videowallorden')// BOTON MOSTRAR ORDEN DE SALIDA
    var Videowallordennextten = document.getElementById('Videowallordennextten')//BOTON MOSTRAR ORDEN DE SALIDA , LOS 10 SIGUIENTES
    var Videowallclasificacion = document.getElementById('Videowallclasificacion')//BOTON DE MOSTRAR LA CLASIFICACION
    var Videowallclasificaciontopten = document.getElementById('Videowallclasificaciontopten')//BOTON CLASIFICAICON TOP 10
    var Videowallmarcador = document.getElementById('Videowallmarcador')//ENVIAMOS EL MARCADOR, JINETE PTS Y TPO
    var Videowallreset = document.getElementById('Videowallreset')//ACTUALIZAMOS PAGINA DE VIDEOWALL

    Videowallreset.addEventListener('click', function (){
            socket.emit('recargarpaginavideowall')    
    })

    Videowallmarcador.addEventListener('click',function (){

     socket.emit('videowallmarcador')
    })
    Videowallorden.addEventListener('click' , function (){
    // alert('lick en class ORDEN')
    socket.emit('videowallorden')
    })

    Videowallordennextten.addEventListener('click' , function (){
        socket.emit('videowallordendiez')

    })

    Videowallclasificacion.addEventListener('click' , function (){
       socket.emit('videowallclasificacion')
       
    })

    Videowallclasificaciontopten.addEventListener('click' , function (){
        socket.emit('videowallTopTen')
        
    })

    Videowallrun.addEventListener('click', function() {
        window.open("http://127.0.0.1:9000/Videowall") 
    })
}



module.exports.iniciarvideowall = function(socket) {
    iniciarvideowall(socket)
}