/*ESTE MODULO SE ENCARGA DE MOSTRAR LAS 
DIFERENTES PANTALLAS DEL VIDEO WALL SEGUN LA ORDEN RECIBIDA POR SOCKET*/
'use strict'

var socket = io()
//recibe por parametro el id de un div a mostrar y el tipo de clase
//oculta todos los elementos con esa clase
//y muestra el div que se pasa por parametro

function divvisible (clase) {

    var elementosaocultar = document.getElementsByClassName(clase)
    for (var i = 0; i < elementosaocultar.length ; i ++) {
        var v = '#' + elementosaocultar[i].id
        if($(v).css('display') == 'block') {
           return (elementosaocultar[i].id)
       }
    }
}
//MOSTRAR LOS 10 MEJORES
socket.on('clasificacionvideowallTopTen', function (){
    var divaocultar = divvisible('videowalldiv')
    $('#' + divaocultar).fadeOut('slow', function() {
        $('#videowallclasificaciontopten').fadeIn('slow')
    })

 })
 socket.on('clasificacionvideowalltodos',function (){

    var divaocultar = divvisible('videowalldiv')
    $('#' + divaocultar).fadeOut('slow', function() {
        $('#videowallclasificaciontodos').fadeIn('slow')
    })

 }) 

//MOSTRAR ORDEN DE SALIDA DE TODA LA PRUEBA
 socket.on('ordenvideowalltodos',function (){
    
    var divaocultar = divvisible('videowalldiv')

    $('#' + divaocultar).fadeOut('slow', function() {
        $('#videowallordentodos').fadeIn('slow')
    })

 })
//MOSTRAR ORDEN DE SALIDA DE LOS SIGUIENTES 10
 socket.on('ordenvideowalldiez',function (){
    var divaocultar = divvisible('videowalldiv')

    $('#' + divaocultar).fadeOut('slow', function() {
        $('#videowallordennextten').fadeIn('slow')
    })
 })


 //MOSTRAR EL MARCADOR DEL JINETE ACTUAL, PTOS TPO , JINETE Y CABALLO
 socket.on('marcadorvideowall', function (){
    var divaocultar = divvisible('videowalldiv')
    $('#' + divaocultar).fadeOut('slow', function() {
        $('#videowallmarcador').fadeIn('slow')
    })
 })
