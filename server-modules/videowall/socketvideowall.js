/* ESTE MODULO SE ENCARGA DEL ENVIO DE LAS ORDENES
A TRAVES DE SOCKET  PARA QUE SE MUESTREN LAS DIFERENTES PANTALLAS
*/

'use strict'

module.exports = function (socket) {

    //videowalltopten > solicita que le enviemos la clasificacion de los 10 mejores
   socket.on('videowallTopTen',function (){
       console.log('recibido videowalltopten, enviando clasificacion a VIDEOWALL')
        socket.broadcast.emit('clasificacionvideowallTopTen') 
              
    }) 
    //videowallclasificacion > solicita que le enviemos la clasificiacion de todos
    socket.on('videowallclasificacion' , function (){
        console.log('RECIBIDO videowallclasificacion')
        socket.broadcast.emit('clasificacionvideowalltodos')
    
    })
    socket.on('videowallorden',function (){
        socket.broadcast.emit('ordenvideowalltodos')
    })

    socket.on('videowallordendiez',function (){
        socket.broadcast.emit('ordenvideowalldiez')
    })

    socket.on('videowallmarcador',function (){
        console.log('RECIBIDO videowallmarcador')

        socket.broadcast.emit('marcadorvideowall')
    })
    socket.on('presentarjinetemarcador',function (){
        socket.broadcast.emit('presentacionjinetemarcador')
    })
    //videowallorden > solicita que le enviemos el orden de toda la prueba

    //videowallorden

}