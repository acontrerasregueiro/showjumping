/* ESTE MODULO SE ENCARGA DEL ENVIO DE LAS ORDENES
A TRAVES DE SOCKET  PARA QUE SE MUESTREN LAS DIFERENTES PANTALLAS
*/

'use strict'

module.exports = function (socket) {
    //ACTUALIZAR EL NOMBRE DEL JINETE Y DEL CABALLO
    socket.on('actualizarjinetemarcador',function(jinete,caballo){
        console.log('recibido actualizarjinetemarcador , enviando jinetecaballomarcador')
        // socket.broadcast.emit('hiderankmarcador')            //NUEVO JINETE BORRAMOS RANKING  

        socket.broadcast.emit('jinetecaballomarcador',jinete,caballo)// MOSTRAMOS NOMBRE JINETE CABALLO
    })    
    socket.on('ocultarpenalidadesmarcador', function (){
        socket.broadcast.emit('hidepenalidadesmarcador')   
    })
    //PARA MOSTRAR PUNTOS , TIEMPO Y RANKING
    socket.on('mostrarpenalidadesmarcador', function (){
        socket.broadcast.emit('showpenalidadesmarcador')              
    })   
    socket.on('puntosmarcador', function (puntos){
        socket.broadcast.emit('actualizarpuntosmarcador',puntos)
    })
    socket.on('tiempomarcador', function (puntos){
        socket.broadcast.emit('actualizartiempomarcador',tiempo)
    })
    socket.on('rankingmarcador', function (rank){
        socket.broadcast.emit('actualizarrankingmarcador',rank) 
    })
    socket.on('ocultarpenalidadesmarcador', function (){
        socket.broadcast.emit('hidepenalidadesmarcador')   
    })
    //PARA MOSTRAR PUNTOS , TIEMPO Y RANKING
    socket.on('mostrarpenalidadesmarcador', function (){
        socket.broadcast.emit('showpenalidadesmarcador')              
    })
    socket.on('mostrarrankmarcador', function (rank){
        socket.broadcast.emit('showrankmarcador',rank)              
    })
    socket.on('ocultarrankmarcador', function (){
        socket.broadcast.emit('hiderankmarcador')              
    })     
}