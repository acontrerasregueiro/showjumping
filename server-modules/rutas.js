'use strict'
//RESUELVE LAS DIRECCIONES DE EXPRESS
module.exports = function(app,path,express) {
  var pathpublic = path.join(__dirname, '../public')
  app.use(express.static(pathpublic))

  app.get('/', function (req, res) {
    var pathclassactiva = path.join(__dirname, './../public/vistas/login.html')
    res.sendFile(pathclassactiva)
  })
  app.get('/modalnuevaprueba', function (req, res) {
    var pathaddjinete = path.join(__dirname, './../public/modalnuevaprueba.html')
    res.sendFile(pathaddjinete)
  })

  app.get('/addinfo', function (req, res) {
    var pathaddjinete = path.join(__dirname, '/public/vistas/jinetes/addinfo.htm')
    res.sendFile(pathaddjinete)
  })
  //Pagina orden de salida 
  app.get('/orden', function (req, res) {
    var pathordensalida = path.join(__dirname, './../public/vistas/ordendesalida.html')
    res.sendFile(pathordensalida)
  })
  //Pagina clasificacion prueba activa
  app.get('/classactiva', function (req, res) {
    var pathclassactiva = path.join(__dirname, './../public/vistas/clasificacionpruebaactiva00.htm')
    res.sendFile(pathclassactiva)
  })
  app.get('/login', function (req, res) {
    var pathclassactiva = path.join(__dirname, './../public/vistas/login.html')
    res.sendFile(pathclassactiva)
  })
  app.get('/videowall', function (req, res) {
    var pathclassactiva = path.join(__dirname, './../public/vistas/videowall/Videowall.html')
    res.sendFile(pathclassactiva)
  })
  app.get('/classtv', function (req, res) {
    var pathclassactiva = path.join(__dirname, './../public/vistas/Display TV/clasificaciontv.htm')
    res.sendFile(pathclassactiva)
  })

  // app.get('/',function(req,res){
    app.post("/", function (req, res) {
      console.log(req.body.user)
      var username = req.body.user
      var password = req.body.pass
      if ((username == 'adrian') && (password =='figura')) {
        var pathindexhtml = path.join(__dirname, './../index2.html')
        res.sendFile(pathindexhtml)
      }else {
        res.redirect('/login')        
      }
  })
  //Anadir jinete prueba en curso
  app.get('/add', function (req, res) {
    var pathclassactiva = path.join(__dirname, './../public/vistas/jinetes/addJinete.htm')
    res.sendFile(pathclassactiva)
  })
}
