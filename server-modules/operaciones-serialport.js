/*ESTE FICHERO REALIZA LAS OPERACIONES SOBRE
EL PUERTO SERIE*/
'use strict'

// function detectarpuertoserie(port) {
//   port.write('CALRT\n')
// }
function analizardatadisplay (datos,socket) {
  var puntos
  var tiempo
  if (datos.substring(0, 1) == 'A') {
    // puntos.innerHTML = ''
    // SI LA CADENA COMIENZA POR A NO HACER NADA

  } else {
   if (datos.charAt(6) == '.') {
    // significa que participante a finalizado O  ESTÁ DURANTE el recorrido
  
    puntos = datos.substring(1, 3)
    var cadenatiempo = datos.substring(7, 19)
    // ELIMINAMOS TODOS LOS ESPACIOS
    cadenatiempo = cadenatiempo.replace(/\s/g, '')
    tiempo = cadenatiempo
  } else {
    cadenatiempo = datos.replace(/\s/g, '')
    tiempo =  cadenatiempo
    puntos = 0
  }
}
  if (puntos != null) { 
    socket.broadcast.emit('puntosmarcador',puntos)
  console.log('enviando puntos ', puntos )
}
if (tiempo != null) {
  console.log('enviando tiempo ', tiempo ) 
  socket.broadcast.emit('tiempomarcador',tiempo)
}
}
function cerrarpuertos(){
  var SerialPort = require('serialport')
  var port = new SerialPort('COM7', {
    baudRate :9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    autoOpen:false,
    rtscts: true
  })
  if (port.isOpen){
    console.log('PORT ISOPEN ', + port.isOpen)
    console.log('puerto abierto cerrandolo....')
    port.close()
  }

  var portDB = new SerialPort('COM10', {
    baudRate :2400,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    autoOpen:false,
    rtscts: true
  })
  if (port.isOpen){
    console.log('PORTDB ISOPEN ', + port.isOpen)
    console.log('puerto abierto cerrandolo DISPLAY BOARD....')
    portDB.close()
  }
} //FIN CERRAR PUERTOS


// module.exports = function(socket) {
//   var SerialPort = require('serialport')
//   const Readline = SerialPort.parsers.Readline

//       const portDB = new SerialPort('COM8', {
//         baudRate :9600,
//         dataBits: 8,
//         parity: 'none',
//         stopBits: 1,
//         autoOpen:false,
//         rtscts: true
//       })
//       const port = new SerialPort('COM5', {
//         baudRate :9600,
//         dataBits: 8,
//         parity: 'none',
//         stopBits: 1,
//         autoOpen:false,
//         rtscts: true
//       })

//       portDB.on('data', function (data) {
//         socket.emit('displayboarddata', data.toString())
//         console.log(+ data.toString())
//       })
//       port.on('data',function (data) {
//         console.log(data.toString())
//         socket.emit('rs232', data)
        
//       })


//       // portDB.on('error', function (err) {
//       //   console.log('Error: port DB : ', err.message)
//       // })
//       // portDB.on('close', function () {
//       // // this is called when you close the port internally
//       // // or the device is unplug externally after connected.
//       //   console.log('portDB Close')
//       // })
//     // })

 
//   socket.on('desconectarpuertoserie', function () {
//     //conexion contendrá "display" o "cronometro"
//     //en funcion del contenido cerramos el puerto correspondiente
//     if ((port.isOpen)== 1){
//       console.log('PORT ISOPEN ', + port.isOpen)
//       console.log('DESCONECTADO')
//       // port.flush(function() {
//         port.close();
//       }
//       if ((portDB.isOpen)== 1){
//         console.log('PORT DB ISOPEN ', + portDB.isOpen)
//         console.log('DESCONECTADO')
//         // port.flush(function() {
//           portDB.close();
//         }
//       // socket.destroy()
//       // port.close()
//     // })
//   })

//   socket.on('conectarpuertoserie', function () {
//     const parserDB = portDB.pipe(new Readline({delimiter: '\r'}))
//     const parser = port.pipe(new Readline({delimiter: '\r'}))
//     // portDB.open(function (err){
//     // if (err) {
//     //   console.log('portDB ERROR OPENING ' + err)
//     // }
//   // })
//     port.open(function (err){
//       if (err) {
//         console.log('port ERROR OPENING ' + err)
//       }
//     })
  
//     // const parser = port.pipe(new Readline({delimiter: '\r'}))
//     // console.log('PORT ISOPEN ', + port.isOpen)
//     // if (port.isOpen){
//     //   console.log('PORT ISOPEN ', + port.isOpen)
      
//     // } else {
//     //   port.open(function (err){
//     //     if (err) {
//     //       console.log(err)
//     //     }
//     // })
    
//     // console.log('')
//     parser.on('data',function (data) {
//       console.log(data)
//       socket.emit('rs232', data)   
//     })
//     parserDB.on('data',function (data) {
//       console.log(data.toString())
//       socket.emit('displayboarddata', data)
      
//     })
//     })

//   // port.on('open', () => console.log('Port open'))



//     // port232.close()
//     // portDB.close()
    

  

// }

/*ESTE FICHERO REALIZA LAS OPERACIONES SOBRE
EL PUERTO SERIE*/
'use strict'

// function detectarpuertoserie(port) {
//   port.write('CALRT\n')
// }

module.exports = function(socket) {
  var SerialPort = require('serialport')
  const Readline = SerialPort.parsers.Readline

      const portDB = new SerialPort('COM10', {
        baudRate :2400,
        dataBits: 8,
        parity: 'none',
        stopBits: 1,
        autoOpen:false,
        rtscts: true
      })
      const port = new SerialPort('COM7', {
        baudRate :9600,
        dataBits: 8,
        parity: 'none',
        stopBits: 1,
        autoOpen:false,
        rtscts: true
      })

      SerialPort.list(function (err, ports) {
        ports.forEach(function (port) {
          console.log('LISTADO PUERTOS  :   ', port.comName)
          if(port.isOpen = 0) {
            console.log('serialportlist > ' + port.isOpen)
            port.close()
          }
          // detectarpuertoserie(port)
        })
      })
      // portDB.on('data', function (data) {
      //   socket.emit('displayboarddata', data)
      //   console.log('Data DISPLAY BOARD : ' + data)
      // })
      // portDB.on('error', function (err) {
      //   console.log('Error: port DB : ', err.message)
      // })
      // portDB.on('close', function () {
      // // this is called when you close the port internally
      // // or the device is unplug externally after connected.
      //   console.log('portDB Close')
      // })
    


  const parser = port.pipe(new Readline({delimiter: '\r'}))
  const parserDB = portDB.pipe(new Readline({delimiter: '\r'}))

  socket.on('conectarpuertoserie', function () {
    
     console.log('checking port')
    if (port.isOpen){
      console.log('PORT ISOPEN ', + port.isOpen)
    } else {
      port.open(function (err){
        if (err) {
          console.log(err)
        }
      })
    }   
    console.log('checking port DB')
    if (portDB.isOpen) {
      console.log('PORTDB ISOPEN ', + portDB.isOpen)
    } else {
      portDB.open(function (err){
        if (err) {
          console.log(err)
        }
      })
    } 
    // console.log('')     
  })

  // port.on('open', () => console.log('Port open'))
  parser.on('data',function (data) {
    console.log(data.toString())
    socket.emit('rs232', data)   
     
  })
  parserDB.on('data',function (data) {
    console.log(data.toString())
    socket.emit('displayboarddata', data)   
    analizardatadisplay(data,socket) 
  })
  console.log('CONECTANDO PUERTO SERIE')

  // socket.on('desconectarpuertoserie', function (conexion) {
  //   //conexion contendrá "display" o "cronometro"
  //   //en funcion del contenido cerramos el puerto correspondiente
  //   if ((port.isOpen)== 1){
  //     console.log('PORT ISOPEN ', + port.isOpen)
  //     console.log('DESCONECTADO')
  //     // port.flush(function() {
  //       port.close();
  //     }
  //     // port.close()
  //   // })
  // })
    // port232.close()
    // portDB.close()
    

  
}

module.exports.iniciarpuertosserie = function() {
  // cerrarpuertos()
}
  


